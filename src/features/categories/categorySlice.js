import { createAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import categoryService from "./categoryService";

// Toutes les catégories
export const getAllCategories = createAsyncThunk(
  "category/getAllCategories",
  async (_, { rejectWithValue }) => {
    try {
      const response = await categoryService.getAllCategorys();
      // Assurez-vous que les données de réponse sont valides avant de les renvoyer
      if (response && response.data) {
        return response.data;
      } else {
        return rejectWithValue("Réponse de l'API invalide");
      }
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);
export const createNewCategory = createAsyncThunk(
  "category/createNewCategory",
  async (newCategory, { rejectWithValue }) => {
    try {
      // Appelez le service pour créer la catégorie
      const response = await categoryService.createCategory(newCategory);

      // Assurez-vous que les données de réponse sont valides avant de les renvoyer
      if (response && response.data) {
        return response.data;
      } else {
        return rejectWithValue("Réponse de l'API invalide", +response);
      }
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const getACategorie = createAsyncThunk(
  "category/getACategory",
  async (id, { rejectWithValue }) => {
    try {
      const response = await categoryService.getCategoryById(id);
      // Retournez les données de la catégorie
      return response.data;
    } catch (error) {
      // Si une erreur se produit, lancez une exception avec le message d'erreur
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

// Créez une fonction pour mettre à jour une catégorie
export const updateCategory = createAsyncThunk(
  "category/update-category",
  async (category) => {
    try {
      return await categoryService.updateCategory(category);
    } catch (error) {
      throw new Error(error);
    }
  }
);

// supprimer
export const deleteCategory = createAsyncThunk(
  "category/delete-category",
  async (id, thunkAPI) => {
    try {
      return await categoryService.deleteCategory(id);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

// Réinitialiser le state
export const resetCategoryState = createAction("category/resetState");

const initialState = {
  categories: [],
  isError: false,
  isSuccess: false,
  isLoading: false,
  errorMessage: "",
};

export const categorySlice = createSlice({
  name: "categories",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getAllCategories.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getAllCategories.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.isError = false;
        state.categories = action.payload;
        state.errorMessage = "";
      })
      .addCase(getAllCategories.rejected, (state, action) => {
        state.isLoading = false;
        state.isSuccess = false;
        state.isError = true;
        state.errorMessage = action.payload;
      })
      .addCase(createNewCategory.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(createNewCategory.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.isError = false;
        // Vous pouvez mettre à jour l'état avec la nouvelle catégorie ici si nécessaire.
        state.categories.push(action.payload); // Par exemple, ajouter la nouvelle catégorie au tableau existant.
        state.errorMessage = "";
      })
      .addCase(createNewCategory.rejected, (state, action) => {
        state.isLoading = false;
        state.isSuccess = false;
        state.isError = true;
        state.errorMessage = action.payload;
      })
      .addCase(getACategorie.pending, (state) => {
        state.isLoading = true;
        state.isError = false;
        state.errorMessage = null;
      })
      .addCase(getACategorie.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.data = action.payload;
      })
      .addCase(getACategorie.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.errorMessage = action.payload; // Stocke le message d'erreur
      })
      .addCase(updateCategory.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(updateCategory.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.updatedCateg = action.payload;
      })
      .addCase(updateCategory.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.errorMessage = action.error;
      })
      .addCase(deleteCategory.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(deleteCategory.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.deletedCat = action.payload;
      })
      .addCase(deleteCategory.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.message = action.error;
      })
      .addCase(resetCategoryState, (state) => {
        return initialState;
      });
  },
});

export default categorySlice.reducer;
