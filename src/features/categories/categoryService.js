import axios from "axios";
import { base_url } from "../../utils/AxiosConfig/base_url";
import requestInterceptor from "../../utils/AxiosConfig/axiosConfig";

export const getAllCategorys = async () => {
  try {
    const response = await axios.get(`${base_url}categorie-chambre/all`);
    // Vérifiez si la réponse a un code de statut HTTP 200 (OK)
    if (response.status == 200) {
      return response; // Les données de la catégorie sont renvoyées
    } else {
      throw new Error(
        "La requête a échoué avec un code de statut: " + response.status
      );
    }
  } catch (error) {
    // Renvoie une erreur avec un message descriptif
    throw new Error(
      "Une erreur s'est produite lors de la récupération des catégories: " +
        error.message
    );
  }
};

// creer
const createCategory = async (categorie) => {
  try {
    const reponse = await axios.post(
      `${base_url}categorie-chambre/create`,
      categorie,
      requestInterceptor
    );
    return reponse;
  } catch (error) {
    console.log(error);
    // throw new Error(
    //   "Une erreur s'est produite lors de la création de la catégorie : " + erreur.message
    // );
  }
};

// selectionner une categorie
const getCategoryById = async (id) => {
  try {
    const response = await axios.get(`${base_url}categorie-chambre/${id}`);
    return response;
  } catch (error) {
    // Si une erreur se produit, récupérez la réponse d'erreur du serveur si elle existe
    if (error.response) {
      // La réponse d'erreur du serveur se trouve dans error.response.data
      throw new Error("Server error: " + JSON.stringify(error.response.data));
    } else {
      // Si l'erreur n'est pas liée à une réponse du serveur, lancez une exception générique
      throw new Error("An error occurred: " + error.message);
    }
  }
};

// modifier une category
const updateCategory = async (category) => {
  try {
    const response = await axios.patch(`${base_url}categorie-chambre/edit/${category.id}`, {
      nom: category.dataCategory.nom,
    });
    return response.data;
  } catch (error) {
    if (error.response) {
      // La réponse d'erreur du serveur se trouve dans error.response.data
      throw new Error("Server error: " + JSON.stringify(error.response.data));
    } else {
      // Si l'erreur n'est pas liée à une réponse du serveur, lancez une exception générique
      throw new Error("An error occurred: " + error.message);
    }
  }
};

//supprimer
const deleteCategory = async (id) => {
  try {
    const response = await axios.delete(`${base_url}categorie-chambre/delete/${id}`);
    return response.data;
  } catch (error) {
    if (error.response) {
      // La réponse d'erreur du serveur se trouve dans error.response.data
      throw new Error("Server error: " + JSON.stringify(error.response.data));
    } else {
      // Si l'erreur n'est pas liée à une réponse du serveur, lancez une exception générique
      throw new Error("An error occurred: " + error.message);
    }
  }
};

const categoryService = {
  getAllCategorys,
  createCategory,
  getCategoryById,
  updateCategory,
  deleteCategory,
};

export default categoryService;
