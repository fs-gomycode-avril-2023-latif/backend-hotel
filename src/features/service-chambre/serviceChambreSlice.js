import { createAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { serviceChambreService } from "./serviceChambreService";

// all
export const getAllService = createAsyncThunk(
  "service/getAllService",
  async (_, { rejectWithValue }) => {
    try {
      const response = await serviceChambreService.getAllService();
      // Assurez-vous que les données de réponse sont valides avant de les renvoyer
      if (response && response.data) {
        return response.data;
      } else {
        return rejectWithValue("Réponse de l'API invalide : ",+response);
      }
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);
export const createNewService = createAsyncThunk(
  "service/createNewService",
  async (newService, { rejectWithValue }) => {
    try {
      // Appelez le service pour créer la catégorie
      const response = await serviceChambreService.createService(newService);

      // Assurez-vous que les données de réponse sont valides avant de les renvoyer
      if (response && response.data) {
        return response.data;
      } else {
        return rejectWithValue("Réponse de l'API invalide", +response);
      }
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const getAService = createAsyncThunk(
  "service/getAService",
  async (id, { rejectWithValue }) => {
    try {
      const response = await serviceChambreService.getServiceById(id);
      // Retournez les données de la catégorie
      return response.data;
    } catch (error) {
      // Si une erreur se produit, lancez une exception avec le message d'erreur
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

// Créez une fonction pour mettre à jour une catégorie
export const updateService = createAsyncThunk(
  "service/update-service",
  async (service) => {
    try {
      return await serviceChambreService.updateService(service);
    } catch (error) {
      throw new Error(error);
    }
  }
);

// supprimer
export const deleteService = createAsyncThunk(
  "service/delete-service",
  async (id, thunkAPI) => {
    try {
      return await serviceChambreService.deleteService(id);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

// Réinitialiser le state
export const resetServiceState = createAction("service/resetState");

const initialState = {
  services: [],
  isError: false,
  isSuccess: false,
  isLoading: false,
  errorMessage: "",
};

export const serviceSlice = createSlice({
  name: "services",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getAllService.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getAllService.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.isError = false;
        state.services = action.payload;
        state.errorMessage = "";
      })
      .addCase(getAllService.rejected, (state, action) => {
        state.isLoading = false;
        state.isSuccess = false;
        state.isError = true;
        state.errorMessage = action.payload;
      })
      .addCase(createNewService.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(createNewService.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.isError = false;
        // Vous pouvez mettre à jour l'état avec la nouvelle catégorie ici si nécessaire.
        state.services.push(action.payload); // Par exemple, ajouter la nouvelle catégorie au tableau existant.
        state.errorMessage = "";
      })
      .addCase(createNewService.rejected, (state, action) => {
        state.isLoading = false;
        state.isSuccess = false;
        state.isError = true;
        state.errorMessage = action.payload;
      })
      .addCase(getAService.pending, (state) => {
        state.isLoading = true;
        state.isError = false;
        state.errorMessage = null;
      })
      .addCase(getAService.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.data = action.payload;
      })
      .addCase(getAService.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.errorMessage = action.payload; // Stocke le message d'erreur
      })
      .addCase(updateService.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(updateService.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.updatedService = action.payload;
      })
      .addCase(updateService.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.errorMessage = action.error;
      })
      .addCase(deleteService.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(deleteService.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.deletedService = action.payload;
      })
      .addCase(deleteService.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.message = action.error;
      })
      .addCase(resetServiceState, (state) => {
        return initialState;
      });
  },
});

export default serviceSlice.reducer;
