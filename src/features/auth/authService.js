// authService.js
import axios from "axios";
import { base_url } from "../../utils/AxiosConfig/base_url";


// Fonction de connexion
export const login = async (credentials) => {
  try {
    // Effectuez une requête POST vers l'API pour l'authentification
    const response = await axios.post(`${base_url}auth/login`, credentials);
    // Si la demande est réussie, renvoyez les données de l'utilisateur (par exemple, le token)
    return response;
  } catch (error) {
    // Si une erreur se produit, renvoyez-la pour la gestion des erreurs dans le composant
    throw new Error('erreur serveur '+error) ;
  }
};

// Autres fonctions d'authentification (par exemple, logout, vérification de la session, etc.) peuvent être ajoutées ici.

const authService = {
  login,
};

export default authService;
