import jwt_decode from 'jwt-decode';

// Fonction pour décoder un token JWT
export const decodeToken = (token) => {
  try {
    // Utilisez la bibliothèque jwt-decode pour décoder le token
    const decodedToken = jwt_decode(token);

    // Les données du token sont maintenant accessibles dans l'objet decodedToken
    return decodedToken;
  } catch (error) {
    // Gérez les erreurs ici si le token est invalide ou s'il y a un autre problème
    console.error('Erreur lors du décodage du token :', error);
    return null;
  }
};


