import React from "react";
import Header from "../../../components/public/Header/Header";
import Footer from "../../../components/public/Footer/Footer";
import { Outlet } from "react-router-dom";
import "./layout.css";

const Layout = () => {
  return (
    <div>
      <div id="layout">
        <div>
          <Header />
        </div>
        <div>
          <Outlet />
        </div>
        <div>
          <Footer />
        </div>
      </div>
    </div>
  );
};

export default Layout;
