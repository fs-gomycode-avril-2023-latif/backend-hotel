export {default as Layout} from "./Layout/Layout"
export {default as Home} from "./Home/Home"
export {default as Contact} from "./Contact/Contact"
export {default as Service} from "./Service/Service"