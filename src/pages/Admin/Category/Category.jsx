import React, { useEffect, useState } from "react";
import { Button, Input, Space, Table, Popconfirm, message } from "antd";
import "./category.css";
import {
  SearchOutlined,
  RedoOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteCategory,
  getAllCategories,
  resetCategoryState,
} from "../../../features/categories/categorySlice";
import { Link, useNavigate } from "react-router-dom";

const Category = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllCategories());
  }, []);

  const dataCategory = useSelector((state) => state.category.categories);
  const [data, setData] = useState(dataCategory); // Initialisez data avec les données de Redux

  const [filteredData, setFilteredData] = useState(data);
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");

  const getColumnSearchProps = (dataIndex) => ({
    // Définissez les filtres de recherche pour la colonne spécifiée
    filterDropdown: ({ setSelectedKeys, selectedKeys }) => (
      <div style={{ padding: 8 }}>
        <Input
          placeholder={`Rechercher ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Rechercher
          </Button>
          <Button
            onClick={() => handleReset()}
            size="small"
            style={{ width: 90 }}
            icon={<RedoOutlined />}
          >
            Réinitialiser
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    render: (text) =>
      searchedColumn === dataIndex ? (
        <span style={{ backgroundColor: "#ffc069" }}>{text}</span>
      ) : (
        text
      ),
  });

  // console.log(data);
  const handleSearch = (selectedKeys, dataIndex) => {
    if (selectedKeys && selectedKeys.length > 0 && selectedKeys[0]) {
      setSearchText(selectedKeys[0]);
      setSearchedColumn(dataIndex);
      const filtered = data.filter((item) => {
        // Vous pouvez définir la logique de filtrage ici
        // Par exemple, si vous voulez filtrer les éléments dont le nom contient searchText
        return item.nom.toLowerCase().includes(selectedKeys[0].toLowerCase());
      });

      // Mettez à jour l'état des données filtrées
      setFilteredData(filtered);
    } else {
      // Gérez le cas où selectedKeys[0] n'est pas défini
      // Par exemple, vous pouvez choisir de ne pas effectuer de filtrage dans ce cas
      // ou de réinitialiser les filtres, etc.
      setSearchedColumn(""); // Réinitialisez searchedColumn si nécessaire
      setSearchText(""); // Réinitialisez searchText si nécessaire
      setFilteredData(data); // Réinitialisez les données filtrées
    }
  };

  const handleReset = () => {
    setSearchText("");
    setSearchedColumn("");

    // Réinitialisez les filtres et les données filtrées ici
    setFilteredData(data);
  };

  const handleDelete = (id) => {
    dispatch(deleteCategory(id)).then((res) => {
      console.log(res);
      if (res.type == "category/delete-category/fulfilled") {
        message.success("Categorie supprime avec success");
        dispatch(resetCategoryState());
        dispatch(getAllCategories());
      } else {
        message.error("Erreur lors de la suppression de la catégorie");
      }
    });
  };

  const columns = [
    {
      title: "Nom",
      dataIndex: "nom",
      key: "id",
      ...getColumnSearchProps("nom"),
    },
    {
      title: "Actions",
      key: "actions",
      render: (text, record) => (
        <span className="d-flex align-items-center">
          <Link
            className="pe-3 text-warning"
            to={`/admin/category/edit-category/${record._id}`}
            type="primary"
          >
            <EditOutlined className="fs-4" />
          </Link>
          <Popconfirm
            title="Êtes-vous sûr de vouloir supprimer cet categorie?"
            onConfirm={() => handleDelete(record._id)}
            okText="Oui"
            cancelText="Non"
          >
            <Link className="text-danger m-0" type="danger">
              <DeleteOutlined className="fs-4" />
            </Link>
          </Popconfirm>
        </span>
      ),
    },
  ];

  return (
    <div>
      <Table dataSource={dataCategory} columns={columns} />
    </div>
  );
};

export default Category;
