export {default as Layout} from "./Layout/ALayout"
export {default as Dashboard} from "./Dashboard/Dashboard"
export {default as Profile} from "./Profile/Profile"
export {default as Facture} from "./Facture/Facture"
export {default as Reservation} from "./Reservation/Reservation"
// user
export {default as User} from "./User/User"
export {default as AddUser} from "./User/AddUser"
export {default as StatutUser} from "./User/StatutUser"
// category
export {default as Category} from "./Category/Category"
export {default as AddCategory} from "./Category/AddCategory"
// service
export {default as Service} from "./ServicePage/Service"
export {default as AddService} from "./ServicePage/AddService"
// chambre
export {default as Chambre} from "./Chambre/Chambre"
export {default as AddChambre} from "./Chambre/AddChambre"