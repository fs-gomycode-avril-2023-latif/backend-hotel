import React from "react";
import { Route, Routes } from "react-router-dom";

import Error from "../../utils/Error/Error";

import {
  Dashboard,
  Profile,
  User,
  AddUser,
  StatutUser,
  Service,
  AddService,
  Category,
  AddCategory,
  Chambre,
  AddChambre,
  Facture,
  Reservation
} from "../Admin";
import ALayout from "./Layout/ALayout";

const AdminRouter = () => {
  return (
    <Routes>
      <Route element={<ALayout />}>
        <Route index element={<Dashboard />} />
        <Route path="profile" element={<Profile />} />
        <Route path="facture" element={<Facture />} />
        <Route path="reservation" element={<Reservation />} />


        {/* utilisateur */}
        <Route path="list-user" element={<User />} />
        <Route path="add-user" element={<AddUser />} />
        <Route path="edit-user/:id" element={<AddUser />} />
        <Route path="statut-user/:id" element={<StatutUser />} />

        {/* categorie */}
        <Route path="category/list-category" element={<Category />} />
        <Route path="category/add-category" element={<AddCategory />} />
        <Route path="category/edit-category/:id" element={<AddCategory />} />
        {/* service */}
        <Route path="service/list-service" element={<Service />} />
        <Route path="service/add-service" element={<AddService />} />
        <Route path="service/edit-service/:id" element={<AddService />} />
        {/* chambre */}
        <Route path="chambre/list-chambre" element={<Chambre />} />
        <Route path="chambre/add-chambre" element={<AddChambre />} />
        <Route path="chambre/edit-chambre/:id" element={<AddChambre />} />

        <Route path="*" element={<Error />} />
      </Route>
    </Routes>
  );
};

export default AdminRouter;
