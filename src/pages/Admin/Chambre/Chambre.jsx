import React, { useEffect, useState } from "react";
import "./chambre.css";
import {
  SearchOutlined,
  RedoOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { Button, Input, Popconfirm, Space, Table, message } from "antd";
import { Link } from "react-router-dom";
import {
  deleteChambre,
  getAllChambre,
  resetChambreState,
} from "../../../features/chambres/chambreSlice";
const Chambre = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllChambre());
  }, []);

  const dataChambre = useSelector((state) => state.chambre.chambres);
  const [data, setData] = useState(dataChambre); // Initialisez data avec les données de Redux
  const [filteredData, setFilteredData] = useState(data);
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");

  const handleSearch = (selectedKeys, dataIndex) => {
    if (selectedKeys && selectedKeys.length > 0 && selectedKeys[0]) {
      setSearchText(selectedKeys[0]);
      setSearchedColumn(dataIndex);
      const filtered = data.filter((item) => {
        // Vous pouvez définir la logique de filtrage ici
        // Par exemple, si vous voulez filtrer les éléments dont le nom contient searchText
        return item.numero
          .toLowerCase()
          .includes(selectedKeys[0].toLowerCase());
      });

      // Mettez à jour l'état des données filtrées
      setFilteredData(filtered);
    } else {
      // Gérez le cas où selectedKeys[0] n'est pas défini
      // Par exemple, vous pouvez choisir de ne pas effectuer de filtrage dans ce cas
      // ou de réinitialiser les filtres, etc.
      setSearchedColumn(""); // Réinitialisez searchedColumn si nécessaire
      setSearchText(""); // Réinitialisez searchText si nécessaire
      setFilteredData(data); // Réinitialisez les données filtrées
    }
  };

  const handleReset = () => {
    setSearchText("");
    setSearchedColumn("");

    // Réinitialisez les filtres et les données filtrées ici
    setFilteredData(data);
  };

  const getColumnSearchProps = (dataIndex) => ({
    // Définissez les filtres de recherche pour la colonne spécifiée
    filterDropdown: ({ setSelectedKeys, selectedKeys }) => (
      <div style={{ padding: 8 }}>
        <Input
          placeholder={`Rechercher ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Rechercher
          </Button>
          <Button
            onClick={() => handleReset()}
            size="small"
            style={{ width: 90 }}
            icon={<RedoOutlined />}
          >
            Réinitialiser
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    render: (text) =>
      searchedColumn === dataIndex ? (
        <span style={{ backgroundColor: "#ffc069" }}>{text}</span>
      ) : (
        text
      ),
  });

  const handleDelete = (id) => {
    dispatch(deleteChambre(id)).then((res) => {
      console.log(res);
      if (res.type == "chambre/delete-chambre/fulfilled") {
        message.success("chambre supprime avec success");
        dispatch(resetChambreState());
        dispatch(getAllChambre());
      } else {
        message.error("Erreur lors de la suppression de la catégorie");
      }
    });
  };

  const columns = [
    {
      title: "Numero de chambre",
      dataIndex: "numero",
      key: "id",
      ...getColumnSearchProps("numero"),
    },
    {
      title: "Categorie",
      dataIndex: "categorie",
      key: "id",
    },
    {
        title: "disponibilie",
        dataIndex: "disponibilite",
        key: "id",
      },
    {
      title: "Prix par nuit",
      dataIndex: "prix_nuit",
      key: "id",
    },
    {
      title: "Actions",
      key: "actions",
      render: (text, record) => (
        <span className="d-flex align-items-center">
          <Link
            className="pe-3 text-warning"
            to={`/admin/chambre/edit-chambre/${record._id}`}
            type="primary"
          >
            <EditOutlined className="fs-4" />
          </Link>
          <Popconfirm
            title="Êtes-vous sûr de vouloir supprimer ce service?"
            onConfirm={() => handleDelete(record._id)}
            okText="Oui"
            cancelText="Non"
          >
            <Link className="text-danger m-0" type="danger">
              <DeleteOutlined className="fs-4" />
            </Link>
          </Popconfirm>
        </span>
      ),
    },
  ];

  return (
    <div>
      <Table dataSource={dataChambre} columns={columns} />
    </div>
  );
};

export default Chambre;
